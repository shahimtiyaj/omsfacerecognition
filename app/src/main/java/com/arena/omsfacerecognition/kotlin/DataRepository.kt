package com.arena.omsfacerecognition.kotlin

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.arena.omsfacerecognition.SimilarityClassifier.Recognition
import com.arena.omsfacerecognition.apiService.ApiClient
import com.arena.omsfacerecognition.apiService.ApiInterface
import com.google.gson.JsonObject
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class DataRepository(val application: Application) {

    val faceRegPushStatus = MutableLiveData<String>()
    private val registered = HashMap<String, Recognition>() //saved Faces


    fun getFaceRegisteredData() {
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getFaceRegisteredData()
        val emptyList = ArrayList<String>()
        //calling the api ---------------------------------------------
        call?.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.isSuccessful) {
                    Log.d("onResponse", "Face Registered data:" + response.body())

                    Log.e("all_json_data", response.body().toString())
                    var json=response.body().toString()

                    //delete backslashes ( \ ) :

                    //delete backslashes ( \ ) :
                    json= json.replace("[\\\\]{1}[\"]{1}".toRegex(), "\"")
                    //delete first and last double quotation ( " ) :
                    //delete first and last double quotation ( " ) :
                    json = json.substring(json.indexOf("{"), json.lastIndexOf("}") + 1)
                    var json1: JSONObject? = null
                    try {
                        json1 = JSONObject(json)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    val sharedPreferences: SharedPreferences = application.applicationContext.getSharedPreferences("HashMap", Context.MODE_PRIVATE)
                    val editor = sharedPreferences.edit()
                    editor.putString("map", json1.toString())
                    Log.e("recognitions_saved", json1.toString())
                    editor.apply()

                } else {
                    Log.d("onResponse", "Face Registered data fail:" + response.body())
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }


    fun postFaceRegisterData(data: String) {
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        Log.e("Data_print", data);
        val call = service?.postFaceRecognitionData(data)
        //calling the api ---------------------------------------------
        call?.enqueue(object : Callback<RegResponse> {
            override fun onResponse(call: Call<RegResponse>, response: Response<RegResponse>) {
                if (response.isSuccessful) {
                    Log.d("onResponse", "Registered Map Data send successful:" + response.body())
                    faceRegPushStatus.postValue(response.body()?.getUserInfo()?.getStatus().toString())
                } else {
                    Log.d("onResponse", "Map data not send:" + response.body())
                }
            }

            override fun onFailure(call: Call<RegResponse>, t: Throwable) {
                Log.d("onFailure1", t.toString())
            }
        })
    }

}