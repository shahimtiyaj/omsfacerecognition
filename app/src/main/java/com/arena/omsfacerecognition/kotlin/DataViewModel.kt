package com.arena.omsfacerecognition.kotlin

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


class DataViewModel(application: Application) : AndroidViewModel(application){

    private val dataRepository = DataRepository(application)
    private var faceRegPushStatus = dataRepository.faceRegPushStatus

    fun getSuccessStatus(): MutableLiveData<String> {
        if (faceRegPushStatus == null) {
            faceRegPushStatus = MutableLiveData()
        }
        return faceRegPushStatus
    }


    fun getFaceRegisterData() {
        dataRepository.getFaceRegisteredData()
    }

    fun postFaceRegisterData(data: String) {
        dataRepository.postFaceRegisterData(data)
    }

}

    class DataViewModelFactory(var application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(DataViewModel::class.java)) {
                return DataViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }
    }
