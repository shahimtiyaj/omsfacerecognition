package com.arena.omsfacerecognition;

public interface SimilarityClassifier {

  /** An immutable result returned by a Classifier describing what was recognized. */
  class Recognition {
    /**
     * A unique identifier for what has been recognized. Specific to the class, not the instance of
     * the object.
     */
    private final String id;
    /** Display name for the recognition. */
    private final String title;
    private String cDate;


    private final Float distance;
    private Object extra;

    public Recognition(
            final String id, final String title, final Float distance, final String cDate) {
      this.id = id;
      this.title = title;
      this.distance = distance;
      this.extra = null;
      this.cDate = cDate;
    }

    public void setExtra(Object extra) {
        this.extra = extra;
    }
    public Object getExtra() {
        return this.extra;
    }

    public void setCdate(String cDate) {
      this.cDate = cDate;
    }
    public String getCdate() {
      return this.cDate;
    }

    @Override
    public String toString() {
      String resultString = "";
      if (id != null) {
        resultString += "[" + id + "] ";
      }

      if (title != null) {
        resultString += title + " ";
      }

      if (distance != null) {
        resultString += String.format("(%.1f%%) ", distance * 100.0f);
      }

      if (cDate != null) {
        resultString += cDate + " ";
      }

      return resultString.trim();
    }

  }
}
