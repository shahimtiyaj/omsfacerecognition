package com.arena.omsfacerecognition.apiService

import com.arena.omsfacerecognition.kotlin.RegResponse
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.PUT

interface ApiInterface {

    @Headers("Content-Type: application/json")
    @GET("get_face_data.json")
    fun getFaceRegisteredData(): Call<JsonObject>

    @Headers("Content-Type: application/json")
    @PUT("put_face_data.json")
    fun postFaceRecognitionData(@Body jsonObject: String): Call<RegResponse>

}


